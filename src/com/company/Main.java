package com.company;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.math.MathContext;
import java.util.Scanner;

public class Main {

    private static int N;  //Загальна кількість гостей

    public static void main(String[] args) {
        boolean wantIT = true;
        while(wantIT){
            try{
                System.out.println("Type a count of guests on the party: ");
                Scanner scan = new Scanner(System.in);
                N = scan.nextInt();
                while(N<0){
                    System.out.println("Negative numbers are not wellcome here! Try again:");
                    N = scan.nextInt();
                }
                System.out.println("The probability that everyone at the party (" + (int)N
                        + " guests except Alice) will hear the rumor before it stops propagating is:");
                System.out.println("Low Accuracy: " + LowAccuracyPosibility(N) );
                System.out.println("High Accuracy: " + HighAccuracyPosibility(N) );
                System.out.println("The Highest Accuracy: " + HighestAccuracyPosibility(N) );
                System.out.println("An Estimate of the expected number of " +
                        "people to hear the rumor is: " + Math.round(PosibleCountGuests(N)));
                System.out.println("Wanna try again? (true | false)");
                wantIT = scan.nextBoolean();
            }catch(Exception e){
                System.out.println("Write a number,please!");
            }
        }
        System.out.println("GoodBye my friend)");
    }
    // Тут реалізовано три способи з різною точністю

    public static double LowAccuracyPosibility(int n){
        if(N==n) n -=2;
        if(n>0){
            if(n==1) return 1. / (N - 2);
            else return (1.* n / (N - 2)) * LowAccuracyPosibility(n - 1);
        }
        else if(n == -1 || n == 0) return 1;
        else return 0;
    }

    public static BigDecimal HighAccuracyPosibility(int n){
        if(N==n) n -=2;
        if(n>0){
            BigDecimal tmp = new BigDecimal((1.* n / (N - 2)));
            if(n==1) return new BigDecimal(1. / (N - 2));
            else return tmp.multiply(HighAccuracyPosibility(n - 1));
        }
        else if(n == -1 || n == 0) return new BigDecimal("1");
        else return new BigDecimal(0);
    }

    // Формула для визначення ймовірності - (n - 2)! / Math.pow(n-2,n-2).
    // Максимальна точність(Контрольована)!
    public static BigDecimal HighestAccuracyPosibility(int N){
        if(N>2){
            BigDecimal tmp = new BigDecimal(Factorial(N-2));
            BigDecimal tmp1 = new BigDecimal(String.valueOf(N-2));
            int Precision = 20000; //точність
            MathContext mc = new MathContext(Precision, RoundingMode.HALF_UP);
            BigDecimal tmp2 = new BigDecimal(String.valueOf(tmp.divide(tmp1.pow(N-2),mc)));
            return tmp2;
        }else if(N==1||N==2){
            return new BigDecimal(N);
        }else{
            return new BigDecimal(0);
        }
    }

    //Функція знаходження факторіалу
    public static BigInteger Factorial(long N){
        if(N==1) return new BigInteger("1");
        BigInteger tmp = new BigInteger(""+N);
        return tmp.multiply(Factorial(--N));
    }

    //Функція для знаходження прибоизної кількості гостей, які почують плітку(Сума всіх окремих ймовірностей)
    public static double PosibleCountGuests(int n){
        if(N==n) n -=2;
        if(n>0){
            if(n==1) return 1. / (N - 2);
            else return (2./(N-3) + 1.* n / (N - 2)) + PosibleCountGuests(n - 1);
        }
        else if(n == -1 || n == 0) return 1;
        else return 0;
    }

}
